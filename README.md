# Steps

## 1
Create a connected app

## 2
Create SSl (Certificate) [https://trailhead.salesforce.com/content/learn/projects/automate-cicd-with-gitlab/integrate-with-gitlab]

## 3
Upload a ssl (certificate)[https://trailhead.salesforce.com/content/learn/projects/automate-cicd-with-gitlab/integrate-with-gitlab] to connected app
sfdx force:auth:logout --targetusername sonic_furqan@resourceful-otter-tpu9wy.com --noprompt

## 4
Auth using (certificate)[https://trailhead.salesforce.com/content/learn/projects/automate-cicd-with-gitlab/integrate-with-gitlab]

## 5
Create Package

## 6
Create ``` .gitlab-ci.yml``` that defines pipline